
ifdef CONFIG_EXTERNAL_RAPIDXML

# Targets provided by this project
.PHONY: rapidxml clean_rapidxml

# Add this to the "external" target
external: rapidxml
clean_external: clean_rapidxml

MODULE_DIR_RAPIDXML=external/required/rapidxml

rapidxml: setup $(BUILD_DIR)/include/rapidxml/rapidxml.hpp
$(BUILD_DIR)/include/rapidxml/rapidxml.hpp:
	@echo
	@echo "==== Installing RapidXML Library ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_DIR_RAPIDXML)/build/$(BUILD_TYPE) ] || \
	        mkdir -p $(MODULE_DIR_RAPIDXML)/build/$(BUILD_TYPE) || \
	        exit 1
	@cd $(MODULE_DIR_RAPIDXML)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
	        cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
	        exit 1
	@cd $(MODULE_DIR_RAPIDXML)/build/$(BUILD_TYPE) && \
	        make -j$(CPUS) install || \
	        exit 1

clean_rapidxml:
	@echo "==== Clean-up RapidXML library ===="
	@[ ! -d $(MODULE_DIR_RAPIDXML)/build ] || \
		rm -rf $(MODULE_DIR_RAPIDXML)/build

else # CONFIG_EXTERNAL_RAPIDXML

rapidxml:
	$(warning $(MODULE_DIR_RAPIDXML) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_RAPIDXML

